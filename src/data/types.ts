export interface Metric {
  id: number;
  name: string;
  timestamp: string;
}

export interface MetricValue {
  value: number;
  timestamp: string;
}
