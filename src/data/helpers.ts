import { Metric, MetricValue } from './types';

// TODO: Make an higher order function here that could build a type-guard for a generic simple object.

export const isValidMetric = (x: unknown): x is Metric => {
  const sampleMetric: Metric = {
    id: 1,
    name: 'foo',
    timestamp: '2021-12-09T17:28:45.987Z',
  };
  const expectedKeys = Object.keys(sampleMetric).sort();
  if (typeof x !== 'object' || x == null) {
    return false;
  }
  const actualKeys = Object.keys(x).sort();
  return arraysEqual(expectedKeys, actualKeys);
};

export const isValidMetricRecordset = (x: unknown): x is MetricValue => {
  const sampleMetric: MetricValue = {
    value: 1,
    timestamp: '2021-12-09T17:28:45.987Z',
  };
  const expectedKeys = Object.keys(sampleMetric).sort();
  if (typeof x !== 'object' || x == null) {
    return false;
  }
  const actualKeys = Object.keys(x).sort();
  return arraysEqual(expectedKeys, actualKeys);
};

function arraysEqual(a: unknown[], b: unknown[]) {
  if (a.length !== b.length) {
    return false;
  }
  for (let i = 0; i < a.length; i++) {
    if (a[i] !== b[i]) {
      return false;
    }
  }
  return true;
}
