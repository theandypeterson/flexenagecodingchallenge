import * as Types from '../types';
import * as Helpers from '../helpers';

// TODO: Test the higher order function metioned in src/data/helpers.ts instead.
describe('isValidMetric', () => {
  it('returns true if all properties are found on the object', () => {
    const metricToValidate: Types.Metric = {
      id: 1,
      name: 'foo',
      timestamp: '2021-12-09T17:28:45.987Z',
    };
    expect(Helpers.isValidMetric(metricToValidate)).toEqual(true);
  });

  it('returns false if any property is missing on the object', () => {
    expect(
      Helpers.isValidMetric({
        name: 'foo',
        timestamp: '2021-12-09T17:28:45.987Z',
      }),
    ).toEqual(false);
    expect(
      Helpers.isValidMetric({
        id: 1,
        timestamp: '2021-12-09T17:28:45.987Z',
      }),
    ).toEqual(false);
    expect(
      Helpers.isValidMetric({
        id: 1,
        name: 'foo',
      }),
    ).toEqual(false);
  });
});
