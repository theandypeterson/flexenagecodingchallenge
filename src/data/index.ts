import React from 'react';
import { isValidMetric, isValidMetricRecordset } from './helpers';
import * as Types from './types';

// TODO: Add this in via config variable
const url = 'http://127.0.0.1:5000/metrics';
const buildMetricUrl = (metricId: number) => `${url}/${metricId}`;
const buildMetricRecordSetUrl = (metricId: number) =>
  `${buildMetricUrl(metricId)}/recordset`;

export const useMetrics = () => {
  const [metrics, setMetrics] = React.useState<Types.Metric[]>([]);
  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const json = await response.json();
        setMetrics(json.metrics);
      } catch (error) {
        console.error('error', error);
      }
    };

    fetchData();
  }, []);
  const createMetric = React.useCallback(async (newMetricName) => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ name: newMetricName }),
    };
    const response = await fetch(url, requestOptions);
    const json = await response.json();
    if (isValidMetric(json)) {
      setMetrics((metrics) => {
        return [...metrics, json];
      });
    }
  }, []);

  const deleteMetric = React.useCallback(async (metricId: number) => {
    const requestOptions = {
      method: 'DELETE',
    };
    const response = await fetch(`${url}/${metricId}`, requestOptions);
    const json = await response.json();
    if (json === 200) {
      setMetrics((metrics) => metrics.filter((x) => x.id !== metricId));
    }
  }, []);
  return { metrics, createMetric, deleteMetric };
};

export const useMetric = (metricId: number | null) => {
  const [metric, setMetric] = React.useState<Types.Metric | null>(null);

  const [metricRecordset, setMetricRecordset] = React.useState<
    Types.MetricValue[] | null
  >(null);
  React.useEffect(() => {
    const fetchData = async () => {
      try {
        if (metricId == null) {
          return;
        }
        const response = await fetch(buildMetricUrl(metricId));
        const json = await response.json();
        setMetric(json);
      } catch (error) {
        console.error('error', error);
      }
    };

    fetchData();
  }, [metricId]);

  React.useEffect(() => {
    const fetchData = async () => {
      try {
        if (metricId == null) {
          return;
        }
        const response = await fetch(buildMetricRecordSetUrl(metricId));
        const json = await response.json();
        setMetricRecordset(json.values);
      } catch (error) {
        console.error('error', error);
      }
    };

    fetchData();
  }, [metricId]);

  const createMetricValue = React.useCallback(
    async (newMetricValue) => {
      if (metricId == null) {
        return;
      }
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ value: newMetricValue }),
        id: metricId,
      };
      const response = await fetch(
        buildMetricRecordSetUrl(metricId),
        requestOptions,
      );
      const json = await response.json();
      if (isValidMetricRecordset(json)) {
        setMetricRecordset((metrics) => {
          if (!metrics) {
            return [json];
          }
          return [...metrics, json];
        });
      }
    },
    [metricId],
  );

  return { metric, metricRecordset, createMetricValue };
};
