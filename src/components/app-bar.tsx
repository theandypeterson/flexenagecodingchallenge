import * as React from 'react';
import { AppBar as MUIAppBar, Toolbar, Typography } from '@mui/material';

type Props = {};
export const AppBar: React.FC<Props> = () => {
  return (
    <MUIAppBar position="sticky">
      <Toolbar>
        <Typography variant="h6" style={{ cursor: 'pointer' }}>
          FlexEngage Metrics
        </Typography>
      </Toolbar>
    </MUIAppBar>
  );
};
