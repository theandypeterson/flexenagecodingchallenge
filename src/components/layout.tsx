import { createTheme, ThemeProvider } from '@mui/material';
import React from 'react';
import { AppBar } from './app-bar';

const theme = createTheme({
  palette: {
    primary: {
      main: '#6a9d54',
    },
    secondary: {
      main: '#000000',
    },
    // error: will use the default color
  },
});

type Props = {};
export const Layout: React.FC<Props> = (props) => {
  return (
    <ThemeProvider theme={theme}>
      <div>
        <AppBar />
        <div
          style={{
            padding: 20,
            backgroundColor: '#ffffff',
          }}
        >
          {props.children}
        </div>
      </div>
    </ThemeProvider>
  );
};
