import { Box, Button, Grid, Paper } from '@mui/material';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { Title } from './metric-title';

type Props = {
  id: number;
  title: string;
  handleDelete: (metricId: number) => void;
};
export const Metric: React.FunctionComponent<Props> = (props) => {
  const { handleDelete, id } = props;

  const onDeleteClick = React.useCallback(() => {
    handleDelete(id);
  }, [handleDelete, id]);

  return (
    <Paper>
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={3}
      >
        <Grid item>
          <Title>{props.title}</Title>
        </Grid>
        <Grid item>
          <Button
            component={Link}
            to={`/metric/${props.id}`}
            variant="contained"
            color="primary"
          >
            Open
          </Button>
        </Grid>
        <Grid item>
          <Button onClick={onDeleteClick} variant="outlined" color="secondary">
            Delete
          </Button>
        </Grid>
        <Grid item>
          <Box />
        </Grid>
      </Grid>
    </Paper>
  );
};
