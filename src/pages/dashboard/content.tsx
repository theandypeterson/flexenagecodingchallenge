import { Grid } from '@mui/material';
import React from 'react';
import 'typeface-roboto';
import * as Types from '../../data/types';
import { Metric } from './metric';

type Props = {
  metrics: Types.Metric[];
  handleDeleteMetric: (metricId: number) => Promise<void>;
};

export const DashboardContent: React.FC<Props> = (props) => {
  return (
    <Grid container spacing={5}>
      {props.metrics
        .sort((a, b) => (a.id > b.id ? 1 : -1))
        .map((metric) => (
          <Grid item xs={12} md={4} key={`metric-${metric.id}`}>
            <Metric
              id={metric.id}
              title={metric.name}
              handleDelete={props.handleDeleteMetric}
            />
          </Grid>
        ))}
    </Grid>
  );
};
