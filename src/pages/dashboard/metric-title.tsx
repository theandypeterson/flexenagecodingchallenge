import { Typography } from '@mui/material';
import React from 'react';

export const Title: React.FC = (props) => {
  return (
    <Typography component="h2" variant="h6" color="secondary">
      {props.children}
    </Typography>
  );
};
