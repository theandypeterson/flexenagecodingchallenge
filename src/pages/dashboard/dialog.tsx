import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@mui/material';
import React from 'react';
import 'typeface-roboto';

type Props = {
  createMetric: (name: string) => void;
  onClose: () => void;
  open: boolean;
};

export const CreateMetricDialog: React.FC<Props> = (props) => {
  const { createMetric } = props;

  const [newMetricName, setNewMetricName] = React.useState(null);

  const onTextFieldChange = React.useCallback((e: any) => {
    const value = e.target?.value;
    setNewMetricName(value);
  }, []);

  const handleCreate = React.useCallback(() => {
    if (newMetricName) {
      createMetric(newMetricName);
    }
  }, [newMetricName, createMetric]);

  return (
    <Dialog open={props.open} onClose={props.onClose}>
      <DialogTitle>Create Metric</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Name"
          fullWidth
          variant="standard"
          onChange={onTextFieldChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={props.onClose}>Cancel</Button>
        <Button
          disabled={newMetricName == null}
          onClick={handleCreate}
          variant="contained"
          color="primary"
        >
          Create
        </Button>
      </DialogActions>
    </Dialog>
  );
};
