import { Box, Button, Grid } from '@mui/material';
import React from 'react';
import 'typeface-roboto';
import { useMetrics } from '../../data';
import { DashboardContent } from './content';
import { CreateMetricDialog } from './dialog';

export const Dashboard = () => {
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const onCreateMetricButtonClick = React.useCallback(() => {
    setIsModalOpen(true);
  }, []);

  const handleClose = React.useCallback(() => {
    setIsModalOpen(false);
  }, []);

  const { metrics, createMetric, deleteMetric } = useMetrics();

  const handleCreate = React.useCallback(
    (name: string) => {
      createMetric(name);
      setIsModalOpen(false);
    },
    [createMetric],
  );

  return (
    <Grid container spacing={2} direction={'column'}>
      <Grid item xs={12}>
        <Button
          onClick={onCreateMetricButtonClick}
          variant="contained"
          color="primary"
        >
          Create Metric
        </Button>
      </Grid>
      <Grid item xs={12}>
        <Box mb={2} />
      </Grid>
      <Grid item xs={12}>
        <DashboardContent metrics={metrics} handleDeleteMetric={deleteMetric} />
      </Grid>
      <CreateMetricDialog
        open={isModalOpen}
        createMetric={handleCreate}
        onClose={handleClose}
      />
    </Grid>
  );
};
