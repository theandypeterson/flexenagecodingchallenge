import * as Helpers from '../helpers';

describe('formatTimestamp', () => {
  it('outputs ', () => {
    expect(Helpers.formatTimestamp('2021-12-09T17:28:45.987Z')).toEqual('12/09/2021 17:28:45');
  });
})

