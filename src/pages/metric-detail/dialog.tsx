import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@mui/material';
import React from 'react';
import 'typeface-roboto';

type Props = {
  createMetricValue: (value: number) => void;
  onClose: () => void;
  open: boolean;
};

export const CreateMetricValueDialog: React.FC<Props> = (props) => {
  const { createMetricValue } = props;
  const [newMetricValue, setNewMetricValue] = React.useState(null);
  const onTextFieldChange = React.useCallback((e: any) => {
    const value = e.target?.value;
    setNewMetricValue(value);
  }, []);

  const handleCreate = React.useCallback(() => {
    if (newMetricValue) {
      createMetricValue(newMetricValue);
    }
  }, [newMetricValue, createMetricValue]);

  return (
    <Dialog open={props.open} onClose={props.onClose}>
      <DialogTitle>Create Metric Value</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin="dense"
          id="value"
          label="Value"
          type="number"
          fullWidth
          variant="standard"
          onChange={onTextFieldChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={props.onClose}>Cancel</Button>
        <Button
          disabled={newMetricValue == null}
          onClick={handleCreate}
          variant="contained"
          color="primary"
        >
          Create
        </Button>
      </DialogActions>
    </Dialog>
  );
};
