import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material';
import React from 'react';
import 'typeface-roboto';
import { MetricValue } from '../../data/types';
import { formatTimestamp } from './helpers';

type Props = {
  metricName: string;
  metricRecordset: MetricValue[];
};

export const MetricRecordsetTable = (props: Props) => {
  return (
    <Grid container direction="column">
      <Grid item>
        <Typography component="h2" variant="h6" color="secondary" gutterBottom>
          {props.metricName}
        </Typography>
      </Grid>
      <Grid item>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Value</TableCell>
                <TableCell>Timestamp</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {props.metricRecordset.map((row, i) => (
                <TableRow
                  key={i}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.value}
                  </TableCell>
                  <TableCell>{formatTimestamp(row.timestamp)}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
};
