import { Typography } from '@mui/material';
import React from 'react';
import { useParams } from 'react-router-dom';
import 'typeface-roboto';
import { useMetric } from '../../data';
import { CreateMetricValueDialog } from './dialog';
import { MetricDetailLayout } from './layout';
import { MetricRecordsetTable } from './table';

export const MetricDetail = () => {
  const params = useParams();
  const metricId = params.metricId ? parseInt(params.metricId, 10) : null;

  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const { metric, createMetricValue, metricRecordset } = useMetric(metricId);
  const handleCreateValue = React.useCallback(() => {
    setIsModalOpen(true);
  }, []);

  const handleClose = React.useCallback(() => {
    setIsModalOpen(false);
  }, []);

  const handleCreate = React.useCallback(
    (newMetricValue) => {
      createMetricValue(newMetricValue);
      setIsModalOpen(false);
    },
    [createMetricValue],
  );

  if (!metric || !metricRecordset) {
    return (
      <MetricDetailLayout handleCreateValue={handleCreateValue}>
        <Typography component="h2" variant="h6" color="secondary" gutterBottom>
          Loading...
        </Typography>
      </MetricDetailLayout>
    );
  }
  return (
    <MetricDetailLayout handleCreateValue={handleCreateValue}>
      <MetricRecordsetTable
        metricName={metric.name}
        metricRecordset={metricRecordset}
      />
      <CreateMetricValueDialog
        open={isModalOpen}
        onClose={handleClose}
        createMetricValue={handleCreate}
      />
    </MetricDetailLayout>
  );
};
