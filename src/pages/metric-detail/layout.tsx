import { Box, Button, Grid } from '@mui/material';
import React from 'react';
import { Link } from 'react-router-dom';
import 'typeface-roboto';

type Props = {
  handleCreateValue: () => void;
};

export const MetricDetailLayout: React.FC<Props> = (props) => {
  return (
    <Grid container direction="column">
      <Grid item>
        <Grid container spacing={3}>
          <Grid item>
            <Button
              component={Link}
              to={`/`}
              variant="outlined"
              color="primary"
            >
              Back to Dashboard
            </Button>
          </Grid>
          <Grid item>
            <Button
              onClick={props.handleCreateValue}
              variant="contained"
              color="primary"
            >
              Add Value
            </Button>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Box mb={3} />
      </Grid>
      <Grid item>{props.children}</Grid>
    </Grid>
  );
};
