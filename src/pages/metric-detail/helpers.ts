export const formatTimestamp = (x: string) => {
  const year = x.substring(0, 4);
  const month = x.substring(5, 7);
  const day = x.substring(8, 10);
  const hour = x.substring(11, 13);
  const min = x.substring(14, 16);
  const sec = x.substring(17, 19);

  return `${month}/${day}/${year} ${hour}:${min}:${sec}`;
};
