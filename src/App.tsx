import React from 'react';
import { Outlet } from 'react-router-dom';
import 'typeface-roboto';
import './App.css';
import { Layout } from './components/layout';

function App() {
  return (
    <Layout>
      <Outlet />
    </Layout>
  );
}

export default App;
